"addon"
{

	"addon_game_name"			"Knights and Castles"
		
	"InstructionsTitle"								"OVERTHROW"
	"InstructionsObjective"							"OBJECTIVE"
	"InstructionsObjectiveText"						"Slay your enemies! Be the first to earn the goal number of kills, or the team with the most kills when the round ends, and you will be victorious."
	"InstructionsHowToPlay"							"HOW TO PLAY"
	"InstructionsLines"								"-Approach the Midas Throne to earn bonus gold and experience.<br>
													-Snatch up any giant coins the Over Boss tosses to earn even more gold.<br>
													-Keep an eye out for item deliveries. An icon will appear on your map where an item will appear.<br>
													-The fewer kills your team has, the better the item you will receive from a delivery.<br>
													-Slay anyone who isn't on your team."

	"Unassigned"      								"Unassigned Team"
	"Spectator"       								"Spectator Team"
	"Neutrals"        								"Wildlings"
	"NoTeam"          								"No Team"

	"DOTA_GoodGuys"   								"Player 1"
	"DOTA_BadGuys"    								"Player 2"
	"DOTA_Custom1"    								"Player 3"
	"DOTA_Custom2"    								"Player 4"
	"DOTA_Custom3"    								"Player 5"
	"DOTA_Custom4"    								"Player 6"
	"DOTA_Custom5"    								"Player 7"
	"DOTA_Custom6"    								"Player 8"
	"DOTA_Custom7"    								"Player 9"
	"DOTA_Custom8"    								"Player 10"
}

