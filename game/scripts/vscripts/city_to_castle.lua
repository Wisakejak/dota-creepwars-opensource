city_to_castle = class({})

--------------------------------------------------------------------------------

function city_to_castle:OnSpellStart()


		
    local caster = self:GetCaster()
	local goldcost = self:GetSpecialValueFor( "gold_cost" ) 

    local playerID = caster:GetMainControllingPlayer() or caster:GetPlayerID() 
    local player = PlayerResource:GetPlayer(playerID)    
    local teamNumber = caster:GetTeamNumber()
	
	if PlayerResource:GetGold(playerID)>=goldcost then   		
		GameRules.AddonData:SetRaceForCastle(caster,GameRules.AddonData:GetUnitRace(player:GetAssignedHero()))					
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------