-- Generated from template
require('utilities')

if DotARisk == nil then
	DotARisk = class({})
end

require( "libraries/timers" )
require( "libraries/util" )

function Precache( context )
	--[[
		Precache things we know we'll use.  Possible file types include (but not limited to):
			PrecacheResource( "model", "*.vmdl", context )
			PrecacheResource( "soundfile", "*.vsndevts", context )
			PrecacheResource( "particle", "*.vpcf", context )
			PrecacheResource( "particle_folder", "particles/folder", context )
	]]
end

-- Create the game mode when we activate
function Activate()
	GameRules.AddonData = DotARisk()
	GameRules.AddonData:InitGameMode()
end

function DotARisk:InitGameMode()
	----print( "[DOTA RTS] Addon loaded." )
	
	-- game Rules
	
	self:ReadGameConfiguration()
	self.gameStarted=false;
	self.validPlayerIDs = {};
	self.playerPopulation = {};
	self.debug=true;
	
	-- dota Rules	
	GameRules:SetUseUniversalShopMode( true )
	GameRules:SetHeroSelectionTime( 120.0 )
	--GameRules:SetPreGameTime( 120.0 )
	GameRules:SetPostGameTime( 600.0 )
	GameRules:SetTreeRegrowTime( 30.0 )
	GameRules:SetGoldTickTime( 999999999.0 )
	GameRules:SetGoldPerTick( 0 )-- Setup rules    
    GameRules:SetHeroRespawnEnabled( false )
    GameRules:SetSameHeroSelectionEnabled( true )
    GameRules:SetUseBaseGoldBountyOnHeroes( false ) -- Need to check legacy values
    GameRules:SetHeroMinimapIconScale( 1 )
    GameRules:SetCreepMinimapIconScale( 1 )
    GameRules:SetRuneMinimapIconScale( 1 )
    GameRules:SetFirstBloodActive( false )
    GameRules:SetHideKillMessageHeaders( true )
    GameRules:EnableCustomGameSetupAutoLaunch( false )
		
    GameMode = GameRules:GetGameModeEntity()  
    --GameMode:SetDamageFilter( Dynamic_Wrap( dotacraft, "FilterDamage" ), self ))
    --GameMode:SetUseCustomHeroLevels ( true )
    GameMode:SetStashPurchasingDisabled( true )
    --GameMode:SetMaximumAttackSpeed( 500 )
    GameMode:SetAnnouncerDisabled( true )    
    GameMode:SetRecommendedItemsDisabled( true )
    GameMode:SetBuybackEnabled( false )	
    GameMode:SetUnseenFogOfWarEnabled( false ) 
    GameMode:SetLoseGoldOnDeath( false )

	self.m_VictoryMessages = {}
	self.m_VictoryMessages[DOTA_TEAM_GOODGUYS] = "#VictoryMessage_GoodGuys"
	self.m_VictoryMessages[DOTA_TEAM_BADGUYS]  = "#VictoryMessage_BadGuys"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_1] = "#VictoryMessage_Custom1"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_2] = "#VictoryMessage_Custom2"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_3] = "#VictoryMessage_Custom3"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_4] = "#VictoryMessage_Custom4"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_5] = "#VictoryMessage_Custom5"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_6] = "#VictoryMessage_Custom6"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_7] = "#VictoryMessage_Custom7"
	self.m_VictoryMessages[DOTA_TEAM_CUSTOM_8] = "#VictoryMessage_Custom8"

	self.m_TeamColors = {}
	self.m_TeamColors[DOTA_TEAM_GOODGUYS] = { 61, 210, 150 }	--		Teal
	self.m_TeamColors[DOTA_TEAM_BADGUYS]  = { 243, 201, 9 }		--		Yellow
	self.m_TeamColors[DOTA_TEAM_CUSTOM_1] = { 197, 77, 168 }	--      Pink
	self.m_TeamColors[DOTA_TEAM_CUSTOM_2] = { 255, 108, 0 }		--		Orange
	self.m_TeamColors[DOTA_TEAM_CUSTOM_3] = { 52, 85, 255 }		--		Blue
	self.m_TeamColors[DOTA_TEAM_CUSTOM_4] = { 101, 212, 19 }	--		Green
	self.m_TeamColors[DOTA_TEAM_CUSTOM_5] = { 129, 83, 54 }		--		Brown
	self.m_TeamColors[DOTA_TEAM_CUSTOM_6] = { 27, 192, 216 }	--		Cyan
	self.m_TeamColors[DOTA_TEAM_CUSTOM_7] = { 199, 228, 13 }	--		Olive
	self.m_TeamColors[DOTA_TEAM_CUSTOM_8] = { 140, 42, 244 }	--		Purple
	
	for team = 0, (DOTA_TEAM_COUNT-1) do
		color = self.m_TeamColors[ team ]
		if color then
			SetTeamCustomHealthbarColor( team, color[1], color[2], color[3] )
		end
	end
	self.playingPlayers=0;
	
	for teamID=DOTA_TEAM_FIRST,self.nTeams+3 do
        GameRules:SetCustomGameTeamMaxPlayers( teamID, 1 )
    end
        GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_NOTEAM, 0 )
        GameRules:SetCustomGameTeamMaxPlayers( DOTA_TEAM_NEUTRALS, 0 )

	
	--[[for id = 0, (DOTA_MAX_TEAM_PLAYERS-1) do
			------print("player ",id," is ",PlayerResource:IsValidPlayerID(id))
		--if PlayerResource:IsValidPlayerID(id) then
			
			PlayerResource:GetPlayer(id):MakeRandomHeroSelection();
		--end
	end]]
	
	self:CreateStartingBuildings();
	GameRules:GetGameModeEntity():SetThink( "OnThink", self, "GlobalThink", 5 )
	ListenToGameEvent('npc_spawned', Dynamic_Wrap(DotARisk, 'OnNPCSpawned'), self)
	ListenToGameEvent('entity_killed', Dynamic_Wrap(DotARisk, 'OnEntityKilled'), self)
end

function DotARisk:CreateStartingBuildings()

	--print("[DOTA RTS] Create Buildings")
	--local RefferenceShop = Entities:FindByName(nil, "refference_shop");

		
	for _,entity in pairs( Entities:FindAllByName( "city")) do
		
		local myTower = CreateUnitByName( "npc_town_basic", entity:GetAbsOrigin(), true, nil, nil, DOTA_TEAM_NEUTRALS )
		--SpawnEntityFromTableSynchronous('trigger_shop', {origin = entity:GetAbsOrigin(), shoptype = 0, model=RefferenceShop:GetModelName()})
		self:SetRaceForCastle(myTower,entity:GetIntAttr("iRace"),true);
		entity:Destroy();
		
	--print ("[DOTA RTS] Created "..(myTower:GetUnitName()))
	end
		
	for _,entity in pairs( Entities:FindAllByName( "tower")) do
		
		CreateUnitByName( "npc_sentry_tower", entity:GetAbsOrigin(), true, nil, nil, DOTA_TEAM_NEUTRALS );
		entity:Destroy();
	end
	--print("[DOTA RTS] Done Creating Buildings")

end

-- A player picked a hero
function DotARisk:InitializePlayer(hero)
	--print("[DOTA RTS] Initialize player",hero)
	local player = hero:GetPlayerOwner();
	    
    hero:SetGold(self.StartingGold, false)
	player.myHero = hero;
	
	self.playerPopulation[player:GetPlayerID()]=0;
	player.numCities = 0;
	player.defeated = false;
	player.elimianted = false;
	self.playingPlayers=self.playingPlayers+1;
	
	--print(player:GetAssignedHero())
	
	local hasCapital = false
	
	--print("[DOTA RTS] Find Valid Town")
	
	local validTowns = {}
	
	for name, town in pairs(Entities:FindAllByClassname("npc_dota_creep_neutral")) do	
			if town:GetUnitName()=="npc_town_basic" and town.PlayerOwner==nil then
				table.insert(validTowns,town)
			end
		end
		--print("[DOTA RTS] Found "..#validTowns.." valid cities")
	--print("[DOTA RTS] Set Capital")
	
	if #validTowns>0 then
	
		local rTown = validTowns[math.ceil(math.random()*#validTowns)];
		
		if rTown~=nil then
			player.numCities = 1;
			rTown:SetControllableByPlayer( player:GetPlayerID(), false );
			rTown:SetOwner( player );
			rTown:SetTeam (player:GetTeam());
			rTown.PlayerOwner=player;	
			self:SetRaceForCastle(rTown,self:GetUnitRace(hero),false);
			self:FocusPlayerCamera(player,rTown:GetAbsOrigin());
									
			local iTeam = rTown.PlayerOwner:GetTeamNumber();						
			rTown:SetRenderColor (GameRules.AddonData.m_TeamColors[iTeam][1],GameRules.AddonData.m_TeamColors[iTeam][2],GameRules.AddonData.m_TeamColors[iTeam][3]);
			
			PlayerResource:SetCameraTarget(player:GetPlayerID(),rTown)
			PlayerResource:SetCameraTarget(player:GetPlayerID(),nil)
		end	
	end
	
	table.insert(self.validPlayerIDs,player:GetPlayerID())
	--print("[DOTA RTS] Player done")
end

function DotARisk:OnNPCSpawned(keys)

	local npc = EntIndexToHScript(keys.entindex)

	if npc:IsRealHero() and npc.bFirstSpawned == nil then
		self:OnHeroInGame(npc)
	
	end
end

function DotARisk:addPop(playerID,popadd)
			--print("[DOTA RTS] Updating population...")
	self.playerPopulation[playerID]=	self.playerPopulation[playerID]+popadd;
	--print("[DOTA RTS] Player "..playerID.." population is now "..self.playerPopulation[playerID])
end 

function DotARisk:OnEntityKilled(keys)

	local killed = EntIndexToHScript(keys.entindex_killed)

	if killed.PlayerOwner~=nil and killed.unitValue>0 then
		--------print (killed.unitValue," substracted")
		self.playerPopulation[killed.PlayerOwner:GetPlayerID()]=self.playerPopulation[killed.PlayerOwner:GetPlayerID()]-killed.unitValue;
	end
end

function DotARisk:DefeatPlayer(player)
	player.elimianted = true;	
	player:GetAssignedHero():AddItemByName("item_revolution");
	
	for iItem = 0, 5 do	
		if player:GetItemInSlot(iItem):GetAbilityName()=="item_revolution" then
		
		player:GetItemInSlot(iItem):StartCooldown(self.RevolutionCD);
		
		end
	end
	
	--------print("[DOTA RTS] Defeated Player")
end

function DotARisk:EliminatePlayer(player)
	player.defeated = true;		
	self.playingPlayers=self.playingPlayers-1;
	--------print("[DOTA RTS] Defeated Player")
end

function DotARisk:OnPlayerDisconnect(keys)
	--------print("[DOTA RTS] Player DCed")
	local player = EntIndexToHScript(keys.player);
	
	if player.numCities == 0 then
	
		self:EliminatePlayer(player);	
	end
end

function DotARisk:OnHeroInGame(hero)
	local hero_name = hero:GetUnitName()
	--print("[DOTA RTS] OnHeroInGame "..hero_name)
	
	--if hero:HasAbility("hide_hero") then
	
		if hero:GetAbilityCount()>0 then
			for iAB = 0, hero:GetAbilityCount()-1 do			
			
				if hero:GetAbilityByIndex(iAB)~=nil then
					local abilityName = hero:GetAbilityByIndex(iAB):GetAbilityName();
					if abilityName~=nil then
						hero:RemoveAbility(abilityName)
					end
				end
			end
		end
		
		--hero:SetAbsOrigin (Vector(0,0,0));		
		self:InitializePlayer(hero);
			
		local ability = hero:AddAbility("hide_hero")
		ability:SetLevel(1)
		hero:SetAbilityPoints(0)
		--Timers:CreateTimer(function() hero:SetAbsOrigin(Vector(position.x,position.y,position.z - 420 )) return 1 end)
		hero:AddNoDraw()
		
		self.armyLimit=math.max(self.armyLimitMin,self.GlobalPopulation/#self.validPlayerIDs);
	
	--end
	--print("[DOTA RTS] Hero done")
end

-- Evaluate the state of the game
function DotARisk:OnThink()
	--print ("[DOTA RTS] On think")
	if GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		
		if self.gameStarted == false then
			self.gameStarted = true;
			
			GameRules:SetCustomVictoryMessage( "#PeaceMessage" )
			
			Timers:CreateTimer(function()
				  self:GoldUpdateCheck();
				  return self.constantGoldCheck;
				end
			  )
		end
		
		self:RecountPlayerCities();
	end
	--print ("[DOTA RTS] Done think")
	return 1
end

--Map Data
function DotARisk:ReadGameConfiguration()
	--print("[DOTA RTS] Reading Game Rules")
	local kv = LoadKeyValues( "scripts/maps/" .. GetMapName() .. ".txt" )
		
	if kv == nil then
		kv = LoadKeyValues( "scripts/maps/default_rules.txt" ) --load the default rules
	end
	
	self.RaceData = kv.RaceData;
	
	if kv.RaceData==nil then	
		self.RaceData = LoadKeyValues( "scripts/maps/default_rules.txt" ).RaceData;
	end	
	
	self.gameSize = "Medium"
	
	self.constantGoldCheck = kv.ConstantGoldCheck;
	self.townGoldIncome = kv.TownGoldIncome;
	self.razeGoldIncome = kv.RazeGoldIncome;
	self.sackGoldIncome = kv.SackGoldIncome;
	self.baseGoldIncome = kv.BaseGoldIncome;
	self.nTeams = kv.MaxPlayers;
	self.armyLimitMin=  kv.ArmyLimit;
	self.armyLimit=  kv.ArmyLimit;
	self.GlobalPopulation=  kv.GlobalPopulation;
	self.StartingGold=  kv.StartingGold;
	
	
	GameRules:SetPreGameTime( kv["PeaceTime"][self.gameSize] )
	
	self.VictoryPercent = kv["VictoryPercent"][self.gameSize];
	self.SpawnTime = kv["SpawnTime"][self.gameSize];
	self.RevolutionCD = kv["SpawnTime"][self.gameSize];
	
	local GameMode = GameRules:GetGameModeEntity()
	
	if kv.vision == 0 then
		
	GameMode:SetFogOfWarDisabled(true)
    GameMode:SetUnseenFogOfWarEnabled(true) 
	
	end if kv.vision == 1 then
	
	GameMode:SetFogOfWarDisabled(false)
    GameMode:SetUnseenFogOfWarEnabled(false) 
	
	end if kv.vision == 2 then
	
	GameMode:SetFogOfWarDisabled(false)
    GameMode:SetUnseenFogOfWarEnabled( true ) 
	
	end
	
	self._bAlwaysShowPlayerGold = kv.AlwaysShowPlayerGold or false
	
end

function DotARisk:FocusPlayerCamera(player,position)
				
	if player:GetAssignedHero()~=nil then
		 player:GetAssignedHero():SetAbsOrigin(position);
	end
end

function DotARisk:RecountPlayerCities()
	
	--print("[DOTA RTS] Recounting Player Cities")
		local towns = 0;
		--print ("Update Cities")
		for name, town in pairs(Entities:FindAllByClassname("npc_dota_creep_neutral")) do
			if town:GetUnitName()=="npc_town_basic" then
				towns=towns+1;						
				if town.PlayerOwner~=nil and town.PlayerOwner.defeated==false then
					town.PlayerOwner.numCities=town.PlayerOwner.numCities+1;
					town.PlayerOwner.elimianted = false;
					town.PlayerOwner.Capital = town;
				end
			end
		end
		
		--print ("Finishing Up")
		for name, myPlayerID in pairs(self.validPlayerIDs) do
			
				local myPlayer = PlayerResource:GetPlayer(myPlayerID)
		
				if myPlayer~=nil and myPlayer.elimianted == false then		
				
					if myPlayer.numCities==0 and self.playerPopulation[myPlayer:GetPlayerID()] == 0 then						
						self:DefeatPlayer(myPlayer);
					elseif myPlayer.numCities>=towns*self.VictoryPercent/100 then					
						self:DoVictory(myPlayer);
					elseif self.playingPlayers<=1 then
						self:DoVictory(myPlayer);
					end					
					
					if myPlayer.numCities==1 and myPlayer.Capital~=nil then
						self:FocusPlayerCamera(myPlayer,myPlayer.Capital:GetAbsOrigin())
					end
					
				end
		end
		--print ("Finished")
end

function DotARisk:DoVictory(myPlayer)
--print("victory!")
if self.debug then return end

		GameRules:SetCustomVictoryMessage( self.m_VictoryMessages[myPlayer:GetTeamNumber()] )
		GameRules:SetGameWinner( myPlayer:GetTeamNumber() )

end

-- Constant timer Update
function DotARisk:GoldUpdateCheck()
	
	--print("[DOTA RTS] Updating Player Gold")
	self:RecountPlayerCities();
	
	------print("[DOTA RTS] Beginning")
	--actual gold	
	for name, myPlayerID in pairs(self.validPlayerIDs) do
	
			local myPlayer = PlayerResource:GetPlayer(myPlayerID)
			if myPlayer~=nil and myPlayer:GetAssignedHero()~=nil then
				myPlayer:GetAssignedHero():ModifyGold(self.townGoldIncome,true,DOTA_ModifyGold_GameTick);
			end
	end
	------print("[DOTA RTS] Middle")
	for name, town in pairs(Entities:FindAllByClassname("npc_dota_creep_neutral")) do
		if town:GetUnitName()=="npc_town_basic" then
			local myPlayer = town.PlayerOwner;
			if myPlayer~=nil and myPlayer:GetAssignedHero()~=nil then
				myPlayer:GetAssignedHero():ModifyGold(self.townGoldIncome,true,DOTA_ModifyGold_GameTick);
			end
		end
	end
	--print("[DOTA RTS] End")
end

function DotARisk:SetRaceForCastle(myUnit,race, game)

	--print("[DOTA RTS] Set Race For Castle")
	if myUnit==nil then
	return end

	local newRace = race;	
		
	if newRace == -1 then
		local rNames = {}
	
		local i=0
		
		for i=0, self.RaceData.nRaces-1 do
		
			if self.RaceData["Race"..tostring(i)]["playerRace"]==0 and self.RaceData["Race"..tostring(i)]["isUsed"]==1 then
		
				table.insert(rNames,i);
			end
		end
		local index = math.ceil(math.random()*(#rNames-1));
		newRace = rNames[index];
		
		--print(#rNames,index,newRace, #self.RaceData)
	end
	
	if myUnit:FindAbilityByName("tower_splashattack")~=nil then
		myUnit:FindAbilityByName("tower_splashattack"):SetLevel(1);
	end
	
	if myUnit.race~=nil then
		myUnit:RemoveAbility(self.RaceData["Race"..tostring(myUnit.race)]["UnitA"]);
		myUnit:RemoveAbility(self.RaceData["Race"..tostring(myUnit.race)]["UnitB"]);
		myUnit:RemoveAbility(self.RaceData["Race"..tostring(myUnit.race)]["General"]);
		myUnit:RemoveAbility("spawn_stop");	
		myUnit:RemoveAbility("city_to_castle");	
	end 
	
	myUnit.race = newRace;
	if game==true then
		myUnit.originalRace = newRace;
	end
	
	--print("Race"..tostring(newRace))
								
	local vrace = self.RaceData["Race"..tostring(myUnit.race)]
	
	myUnit:AddAbility(vrace["UnitA"]);
	if myUnit:FindAbilityByName(vrace["UnitA"])~=nil then
	myUnit:FindAbilityByName(vrace["UnitA"]):SetLevel(1);
	end
		
	myUnit:AddAbility(vrace["UnitB"]);
	if myUnit:FindAbilityByName(vrace["UnitB"])~=nil then
	myUnit:FindAbilityByName(vrace["UnitB"]):SetLevel(1);
	end
			
	if vrace.playerRace==1 then		
		local myAbility = myUnit:AddAbility(vrace["General"]);
		if myAbility~=nil then
			myAbility:SetLevel(1);
		end
	else 	
		local myAbility = myUnit:AddAbility("city_to_castle");		
		if myAbility~=nil then
			myAbility:SetLevel(1);		
		end
	end
	--myUnit:AddAbility("spawn_stop"):SetLevel(1);
	
	--print("[DOTA RTS] Done")
end

function DotARisk:GetUnitRace(unit)

	if unit==nil then
		return -1	
	end
	
  if (unit:GetUnitName()=="npc_dota_hero_legion_commander") then
	return 0;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_gyrocopter") then
	return 1;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_luna") then
	return 2;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_shredder") then
	return 3;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_huskar") then
	return 4;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_broodmother") then
	return 5;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_doom_bringer") then
	return 6;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_slardar") then
	return 7;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_furion") then
	return 8;
  end
	
  if (unit:GetUnitName()=="npc_dota_hero_enchantress") then
	return 9;
  end
  
  return -1;
  
end
