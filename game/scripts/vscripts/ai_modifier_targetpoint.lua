ai_modifier_targetpoint = class({})

--------------------------------------------------------------------------------

function ai_modifier_targetpoint:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function ai_modifier_targetpoint:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function ai_modifier_targetpoint:OnCreated( kv )
				
	self.searchradius = self:GetCaster():GetCurrentVisionRange()	
	
	
	if self:GetCaster().GetAbilityByIndex~=nil then
		for iAB = 0, 15 do	
			
					local ability = self:GetCaster():GetAbilityByIndex(iAB);				
					
					if ability then
	
						local behavior = ability:GetBehavior()
									
						if bit.band(behavior, DOTA_ABILITY_BEHAVIOR_AUTOCAST) ~= 0 then
														
							if bit.band(behavior, DOTA_ABILITY_BEHAVIOR_POINT) ~= 0 or bit.band(behavior, DOTA_ABILITY_BEHAVIOR_OPTIONAL_POINT) ~= 0 then
								
								ability:ToggleAutoCast();
							break;
							end
						end
					end					
		end			
	end
end

--------------------------------------------------------------------------------

function ai_modifier_targetpoint:DeclareFunctions()
		
	return {MODIFIER_EVENT_ON_ABILITY_START  }
end

--------------------------------------------------------------------------------


function ai_modifier_targetpoint:OnAbilityStart(event)
				
	if bit.band(event.ability:GetBehavior(),DOTA_ABILITY_BEHAVIOR_POINT)~=0 then

		local unitsoftype = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetOrigin(), nil, self.searchradius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, 0, false )
						
			for _,unit in pairs(unitsoftype) do
			
				if unit~=self:GetCaster() then 
				
				local ab = unit:FindAbilityByName(event.ability:GetName())
				
					if ab~=nil and ab:GetAutoCastState() and ab:IsFullyCastable() then
					
						local vCaster = self:GetCaster():GetOrigin();
						local vPoint = event.ability:GetCursorPosition ()
					
						local final_point = vPoint+vCaster-unit:GetOrigin();
					
						 unit:CastAbilityOnPosition(final_point,unit:FindAbilityByName(event.ability:GetName()), self:GetCaster():GetPlayerOwnerID())
					end
				end
			end
	end
end
