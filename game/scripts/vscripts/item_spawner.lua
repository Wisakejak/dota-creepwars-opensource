function SpawnUnit ( kv )
		
    local caster = kv.caster
    local ability = kv.ability

    local unitspawned = ability:GetAbilityKeyValues()['UnitName']
	local unitcost = ability:GetSpecialValueFor("ItemUnitCost") or 0
	local unitpopulation = ability:GetSpecialValueFor("ItemUnitPopulation") or 1

    local playerID = caster:GetMainControllingPlayer() or caster:GetPlayerID() 
    local player = PlayerResource:GetPlayer(playerID)    
    local teamNumber = caster:GetTeamNumber()

	--print("preparing to create ",unitspawned)
	
	if ability:GetCooldownTimeRemaining()<=0 and PlayerResource:GetGold(playerID)>=unitcost and GameRules.AddonData.playerPopulation[player:GetPlayerID()]+unitpopulation<=GameRules.AddonData.armyLimit then
   		--print("creating ",unitspawned)
		
		local uSummon = CreateUnitByName( unitspawned, caster:GetCenter(), true, caster, caster:GetOwner(), caster:GetTeamNumber() )
		if uSummon ~= nil then
			--print("created ",unitspawned)
			uSummon:SetControllableByPlayer( playerID, false )
			uSummon:SetOwner( caster )

			uSummon.PlayerOwner = player;
			uSummon.unitValue=unitpopulation;
			
			GameRules.AddonData:addPop(uSummon.PlayerOwner:GetPlayerID(),unitpopulation);
			PlayerResource:SpendGold(playerID, unitcost,DOTA_ModifyGold_PurchaseItem)
			
			ability:StartCooldown(GameRules.AddonData.SpawnTime);
		end
		
	end
	
	if not ability:GetAutoCastState() then
		caster:RemoveModifierByName("modifier_autospawn")
	end
end