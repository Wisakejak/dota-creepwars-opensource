ai_modifier_targetunit = class({})

--------------------------------------------------------------------------------

function ai_modifier_targetunit:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function ai_modifier_targetunit:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function ai_modifier_targetunit:OnCreated( kv )
	
	self.castAbility = nil
	
	if self:GetCaster().GetAbilityByIndex~=nil then
		for iAB = 0, 15 do	
			
					local ability = self:GetCaster():GetAbilityByIndex(iAB);				
					
					if ability then
	
						local behavior = ability:GetBehavior()
									
						if bit.band(behavior, DOTA_ABILITY_BEHAVIOR_AUTOCAST) ~= 0 then
																
							if bit.band(behavior, DOTA_ABILITY_BEHAVIOR_UNIT_TARGET) ~= 0 or bit.band(behavior, DOTA_ABILITY_BEHAVIOR_OPTIONAL_UNIT_TARGET) ~= 0 then
								
							self.castAbility=ability;
							ability:ToggleAutoCast();
							break;
							
							end
						end
					end					
		end			
	end
end

--------------------------------------------------------------------------------

function ai_modifier_targetunit:DeclareFunctions()
		
	return {MODIFIER_EVENT_ON_ATTACK_START  }
end

--------------------------------------------------------------------------------



function ai_modifier_targetunit:OnAttackStart()
	
	local caster = self:GetCaster();
	
					local ability = self.castAbility;
					
					if ability~=nil and ability:IsFullyCastable() and ability:GetAutoCastState() then
	
					
							
							local target
							local enemies = FindUnitsInRadius( caster:GetTeamNumber(), caster:GetOrigin(), nil, ability:GetCastRange(), ability:GetAbilityTargetTeam() or DOTA_UNIT_TARGET_TEAM_ENEMY, ability:GetAbilityTargetType(), ability:GetAbilityTargetFlags(), 0, false )
							
							--local searchmodifier = tostring(ability:GetSpecialValueFor( "ai_autocast_modifier" )) or ""
							
							--print (searchmodifier)
							
							for k,unit in pairs(enemies) do
								--if not caster:HasModifier(searchmodifier) then
								if math.random()*1<0.25 then
									target = unit
									break
								end
							end

							if target then
								caster:CastAbilityOnTarget(target, ability, caster:GetPlayerOwnerID())
								ExecuteOrderFromTable(
								{
									UnitIndex = caster:entindex(), 
									OrderType = DOTA_UNIT_ORDER_ATTACK,
									TargetIndex = target:entindex(),
									Queue =1
								})
							end
							
					
					end		
end

--[[------------------------------------------------------------------------------

function ai_modifier_targetunit:OnIntervalThink( kv )
	
	local caster = self:GetCaster();
	
					local ability = self.castAbility;
					
					if ability~=nil and ability:ability:IsFullyCastable() and ability:GetAutoCastState() then
	
						local behavior = ability:GetBehavior()
						local behaviorTable = SplitBehaviors(behavior)
						
						if behaviorTable["DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"] then
							
							local target
							local enemies = FindUnitsInRadius( caster:GetTeamNumber(), caster:GetOrigin(), nil, ability:GetCastRange(), ability:GetAbilityTargetTeam(), ability:GetAbilityTargetFlags(), ability:GetAbilityTargetType(), 0, false )
							
							local searchmodifier = ability:GetSpecialValueFor( "ai_autocast_modifier" ) or ""
							
							for k,unit in pairs(enemies) do
								if not IsCustomBuilding(unit) and not caster:HasModifier(searchmodifier) then
									target = unit
									break
								end
							end

							if target then
								caster:CastAbilityOnTarget(target, ability, caster:GetPlayerOwnerID())
							end
							
						end
					
					end				
end
]]--