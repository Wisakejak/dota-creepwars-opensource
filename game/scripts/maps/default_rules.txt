"default_rules"
{
	"ConstantGoldCheck"		"30"
	"TownGoldIncome"		"1"
	"RazeGoldIncome"		"2"
	"SackGoldIncome"		"0"
	"BaseGoldIncome"		"2"
	"StartingGold"			"10"
	"MaxPlayers"			"10"
	"ArmyLimit"				"50"
	"GlobalPopulation"		"400"
	"vision"				"0"
	
	"VictoryPercent"
	{
		"Small"		"55"
		"Medium"	"70"
		"Large"		"85"
	}
	
	"PeaceTime"
	{
		"Small"		"5"
		"Medium"	"30"
		"Large"		"60"
	}
	"SpawnTime"		
	{
		"Small"		"0.1"
		"Medium"	"0.25"
		"Large"		"0.5"
	}
	"RevolutionCD"	
	{
		"Small"		"120"
		"Medium"	"60"
		"Large"		"30"
	}
		
	"RaceData"
	{
		"nRaces"	"22"
		"Race0"
		{
			"UnitA"	"spawn_human_light"
			"UnitB"	"spawn_human_heavy"
			"General"	"spawn_human_general"
			"playerRace"	"1"
		}
		"Race1"
		{
			"UnitA"	"spawn_dwarf_light"
			"UnitB"	"spawn_dwarf_heavy"
			"General"	"spawn_dwarf_general"
			"playerRace"	"1"
		}
		"Race2"
		{
			"UnitA"	"spawn_elf_light"
			"UnitB"	"spawn_elf_heavy"
			"General"	"spawn_elf_general"
			"playerRace"	"1"
		}
		"Race3"
		{
			"UnitA"	"spawn_goblin_light"
			"UnitB"	"spawn_goblin_heavy"
			"General"	"spawn_goblin_general"
			"playerRace"	"1"
		}
		"Race4"
		{
			"UnitA"	"spawn_orc_light"
			"UnitB"	"spawn_orc_heavy"
			"General"	"spawn_orc_general"
			"playerRace"	"1"
		}
		"Race5"
		{
			"UnitA"	"spawn_spider_light"
			"UnitB"	"spawn_spider_heavy"
			"General"	"spawn_spider_general"
			"playerRace"	"1"
		}
		"Race6"
		{
			"UnitA"	"spawn_demon_light"
			"UnitB"	"spawn_demon_heavy"
			"General"	"spawn_demon_general"
			"playerRace"	"1"
		}
		"Race7"
		{
			"UnitA"	"spawn_naga_light"
			"UnitB"	"spawn_naga_heavy"
			"General"	"spawn_naga_general"
			"playerRace"	"1"
		}
		"Race8"
		{
			"UnitA"	"spawn_shroom_light"
			"UnitB"	"spawn_shroom_heavy"
			"General"	"spawn_shroom_general"
			"playerRace"	"1"
		}
		"Race9"
		{
			"UnitA"	"spawn_forest_light"
			"UnitB"	"spawn_forest_heavy"
			"General"	"spawn_forest_general"
			"playerRace"	"1"
		}
		"Race10"
		{
			"UnitA"	"spawn_ogre_light"
			"UnitB"	"spawn_ogre_heavy"
			"playerRace"	"0"
			"isUsed"		"0"
		}
		"Race11"
		{
			"UnitA"	"spawn_dead_light"
			"UnitB"	"spawn_dead_heavy"
			"playerRace"	"0"
			"isUsed"		"1"
		}
		"Race12"
		{
			"UnitA"	"spawn_spectre_light"
			"UnitB"	"spawn_spectre_heavy"
			"playerRace"	"0"
			"isUsed"		"0"
		}
		"Race13"
		{
			"UnitA"	"spawn_golem_light"
			"UnitB"	"spawn_golem_heavy"
			"playerRace"	"0"
			"isUsed"		"1"
		}
		"Race14"
		{
			"UnitA"	"spawn_lizard_light"
			"UnitB"	"spawn_lizard_heavy"
			"playerRace"	"0"
			"isUsed"		"1"
		}
		"Race15"
		{
			"UnitA"	"spawn_satyr_light"
			"UnitB"	"spawn_satyr_heavy"
			"playerRace"	"0"
			"isUsed"		"0"
		}
		"Race16"
		{
			"UnitA"	"spawn_windkin_light"
			"UnitB"	"spawn_windkin_heavy"
			"playerRace"	"0"
			"isUsed"		"0"
		}
		"Race17"
		{
			"UnitA"	"spawn_wolf_light"
			"UnitB"	"spawn_wolf_heavy"
			"playerRace"	"0"
			"isUsed"		"0"
		}
		"Race18"
		{
			"UnitA"	"spawn_bear_light"
			"UnitB"	"spawn_bear_heavy"
			"playerRace"	"0"
			"isUsed"		"0"
		}
		"Race19"
		{
			"UnitA"	"spawn_ursa_light"
			"UnitB"	"spawn_ursa_heavy"
			"playerRace"	"0"
			"isUsed"		"1"
		}
		"Race20"
		{
			"UnitA"	"spawn_harpy_light"
			"UnitB"	"spawn_harpy_heavy"
			"playerRace"	"0"
			"isUsed"		"1"
		}
		"Race21"
		{
			"UnitA"	"spawn_wight_light"
			"UnitB"	"spawn_wight_heavy"
			"playerRace"	"0"
			"isUsed"		"1"
		}
	}
}