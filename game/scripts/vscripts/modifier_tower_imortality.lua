modifier_tower_imortality = class({})

--------------------------------------------------------------------------------

function modifier_tower_imortality:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_tower_imortality:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_tower_imortality:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
		MODIFIER_PROPERTY_HEALTH_REGEN_CONSTANT,
		MODIFIER_PROPERTY_MIN_HEALTH 
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_tower_imortality:GetModifierConstantHealthRegen()

	if not self:GetCaster():HasModifier("modifier_tower_stopregen") then
		return self.selfHeal;
	end
	return 0;
end

--------------------------------------------------------------------------------

function modifier_tower_imortality:GetMinHealth()
	return 1
end

--------------------------------------------------------------------------------

function modifier_tower_imortality:OnCreated( kv )
	
	self.selfHeal = self:GetAbility():GetSpecialValueFor( "heal_regen" );
	self.selfHealInterval = self:GetAbility():GetSpecialValueFor( "heal_interval" );
	self.respawn_life = self:GetAbility():GetSpecialValueFor( "respaw_life" );
	
	self.goldRatio = GameRules.razeGoldIncome;
	self.base_gold=GameRules.sackGoldIncome; 
end

--------------------------------------------------------------------------------

function modifier_tower_imortality:CheckState()
	local state = {
	[MODIFIER_STATE_NO_TEAM_SELECT] = true
	}

	return state
end

--------------------------------------------------------------------------------

function modifier_tower_imortality:OnTakeDamage ( params )
		if IsServer() then
			if self:GetCaster() == nil then
				return 0
			end

			if self:GetCaster() ~= self:GetParent() then
				return 0
			end
			if self:GetCaster().PlayerOwner==nil or (self:GetCaster():GetHealth()==1 and GameRules:State_Get() ~= DOTA_GAMERULES_STATE_PRE_GAME) then						
				
				if self:GetCaster().PlayerOwner~=nil then
					GameRules.AddonData.FocusPlayerCamera(self:GetCaster():GetPlayerOwner(),self:GetCaster():GetAbsOrigin())
				end
				
				local hAttacker = params.attacker
				local hVictim = params.unit

				if hVictim ~= nil and hAttacker ~= nil and hVictim == self:GetCaster() then
					
					hVictim:SetHealth(hVictim:GetMaxHealth()*self.respawn_life);					
					
					local Player = hAttacker:GetPlayerOwner();
					
					if Player==nil then 
						Player = hAttacker.PlayerOwner;
					end
					
					if Player~=nil then
						hVictim:SetControllableByPlayer( Player:GetPlayerID(), false );
						hVictim:SetOwner( Player );
						hVictim:SetTeam (Player:GetTeam());
						hVictim.PlayerOwner=Player;
												
						local iTeam = hVictim.PlayerOwner:GetTeamNumber();						
						hVictim:SetRenderColor (GameRules.AddonData.m_TeamColors[iTeam][1],GameRules.AddonData.m_TeamColors[iTeam][2],GameRules.AddonData.m_TeamColors[iTeam][3]);
					end
					
					-- SELL ALL MY ITEMS -- 
					--[[for Zim = 0, 5 do
					
						local itemN = hVictim:GetItemInSlot(Zim);
						local GoldValue = 0;
						
						if itemN~=nil then					
							GoldValue=GoldValue+itemN:GetCost();				
							itemN:Destroy();
						end		

						local myPlayer = hAttacker:GetPlayerOwner();
					end		]]

					local GoldValue=0;
					if hVictim.race~=hVictim.originalRace then
						GoldValue=1;
						GameRules.AddonData:SetRaceForCastle(hVictim,hVictim.originalRace,false);
					end
					
					if myPlayer~=nil and myPlayer:GetAssignedHero()~=nil then
						myPlayer:GetAssignedHero():ModifyGold(GoldValue*self.goldRatio+self.base_gold,false,DOTA_ModifyGold_Building);
					end
				end
			else
			
				if params.damage>1 and params.unit == self:GetCaster()  then
				self:GetCaster():AddNewModifier( self:GetCaster(), self, "modifier_tower_stopregen", { duration = self.selfHealInterval } )
				end
			end
		end

end
	
--------------------------------------------------------------------------------