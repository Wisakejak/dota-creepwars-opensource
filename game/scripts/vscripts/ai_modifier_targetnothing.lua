ai_modifier_targetnothing = class({})

--------------------------------------------------------------------------------

function ai_modifier_targetnothing:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function ai_modifier_targetnothing:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function ai_modifier_targetnothing:OnCreated( kv )
				
	self.searchradius = self:GetCaster():GetCurrentVisionRange()
	
	self.state = nil;
	self.castAbility = nil
	
	if self:GetCaster().GetAbilityByIndex~=nil then
		for iAB = 0, 15 do	
			
					local ability = self:GetCaster():GetAbilityByIndex(iAB);				
					
					if ability then
	
						local behavior = ability:GetBehavior()
									
						if bit.band(behavior, DOTA_ABILITY_BEHAVIOR_AUTOCAST) ~= 0 then
															
							self.castAbility=ability;
							ability:ToggleAutoCast();
							break;
						end
					end					
		end			
	end
end

--------------------------------------------------------------------------------

function ai_modifier_targetnothing:DeclareFunctions()
		
	return {MODIFIER_EVENT_ON_ABILITY_EXECUTED  }
end

--------------------------------------------------------------------------------


function ai_modifier_targetnothing:OnAbilityExecuted(event)
	
	if bit.band(event.ability:GetBehavior(),DOTA_ABILITY_BEHAVIOR_NO_TARGET)~=0 then

		local unitsoftype = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetOrigin(), nil, self.searchradius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_CREEP, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, 0, false )
			
		if #unitsoftype > 0 then
			for _,unit in pairs(unitsoftype) do
			
			local ab = unit:FindAbilityByName(event.ability:GetName())
			
				if ab~=nil and ab:GetAutoCastState() and ab:IsFullyCastable() then
				
					 unit:CastAbilityNoTarget(ab, self:GetCaster():GetPlayerOwnerID())
				end
			end
		end
	
	end
end
